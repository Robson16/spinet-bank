<?php

/**
 * Register Compaines Custom Post Type
 */

function spinetbank_companies() {

	$labels = array(
		'name'                  => _x( 'Companies', 'Post Type General Name', 'spinetbank' ),
		'singular_name'         => _x( 'Company', 'Post Type Singular Name', 'spinetbank' ),
		'menu_name'             => __( 'Companies', 'spinetbank' ),
		'name_admin_bar'        => __( 'Company', 'spinetbank' ),
		'archives'              => __( 'Companies Archives', 'spinetbank' ),
		'attributes'            => __( 'Companies Attributes', 'spinetbank' ),
		'parent_item_colon'     => __( 'Parent Company:', 'spinetbank' ),
		'all_items'             => __( 'All Companies', 'spinetbank' ),
		'add_new_item'          => __( 'Add New Company', 'spinetbank' ),
		'add_new'               => __( 'Add Company', 'spinetbank' ),
		'new_item'              => __( 'New Company', 'spinetbank' ),
		'edit_item'             => __( 'Edit Company', 'spinetbank' ),
		'update_item'           => __( 'Update Company', 'spinetbank' ),
		'view_item'             => __( 'View Company', 'spinetbank' ),
		'view_items'            => __( 'View Companies', 'spinetbank' ),
		'search_items'          => __( 'Search Company', 'spinetbank' ),
		'not_found'             => __( 'Not found', 'spinetbank' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'spinetbank' ),
		'featured_image'        => __( 'Featured Image', 'spinetbank' ),
		'set_featured_image'    => __( 'Set featured image', 'spinetbank' ),
		'remove_featured_image' => __( 'Remove featured image', 'spinetbank' ),
		'use_featured_image'    => __( 'Use as featured image', 'spinetbank' ),
		'insert_into_item'      => __( 'Insert into company', 'spinetbank' ),
		'uploaded_to_this_item' => __( 'Uploaded to this company', 'spinetbank' ),
		'items_list'            => __( 'Companies list', 'spinetbank' ),
		'items_list_navigation' => __( 'Companies list navigation', 'spinetbank' ),
		'filter_items_list'     => __( 'Filter companies list', 'spinetbank' ), 
	);
	$rewrite = array(
		'slug'                  => 'companies',
		'with_front'            => false,
	);
	$args = array(
		'label'                 => __( 'Company', 'spinetbank' ),
		'description'           => __( 'Partner companies', 'spinetbank' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', 'author', 'revisions' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-building',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		'rewrite'               => $rewrite,
		'show_in_rest'          => true,
	);
	register_post_type( 'companies', $args );

}
add_action( 'init', 'spinetbank_companies', 0 );
