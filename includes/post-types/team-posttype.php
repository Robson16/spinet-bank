<?php 

/**
 * Register Team Custom Post Type
 * 
 * 'supports'              => array( 'title', 'excerpt', 'thumbnail', 'author', 'revisions' ),
 */

// Register Custom Post Type
function spinetbank_team() {

	$labels = array(
		'name'                  => _x( 'Team', 'Post Type General Name', 'spinetbank' ),
		'singular_name'         => _x( 'Team', 'Post Type Singular Name', 'spinetbank' ),
		'menu_name'             => __( 'Team', 'spinetbank' ),
		'name_admin_bar'        => __( 'Team Member', 'spinetbank' ),
		'archives'              => __( 'Team Archives', 'spinetbank' ),
		'attributes'            => __( 'Team Member Attributes', 'spinetbank' ),
		'parent_item_colon'     => __( 'Parent Team Member:', 'spinetbank' ),
		'all_items'             => __( 'All Team Members', 'spinetbank' ),
		'add_new_item'          => __( 'Add New Team Member', 'spinetbank' ),
		'add_new'               => __( 'Add Team Member', 'spinetbank' ),
		'new_item'              => __( 'New Team Member', 'spinetbank' ),
		'edit_item'             => __( 'Edit Team Member', 'spinetbank' ),
		'update_item'           => __( 'Update Team Member', 'spinetbank' ),
		'view_item'             => __( 'View Team Member', 'spinetbank' ),
		'view_items'            => __( 'View Team Members', 'spinetbank' ),
		'search_items'          => __( 'Search Team Member', 'spinetbank' ),
		'not_found'             => __( 'Not found', 'spinetbank' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'spinetbank' ),
		'featured_image'        => __( 'Featured Image', 'spinetbank' ),
		'set_featured_image'    => __( 'Set featured image', 'spinetbank' ),
		'remove_featured_image' => __( 'Remove featured image', 'spinetbank' ),
		'use_featured_image'    => __( 'Use as featured image', 'spinetbank' ),
		'insert_into_item'      => __( 'Insert into team member', 'spinetbank' ),
		'uploaded_to_this_item' => __( 'Uploaded to this team member', 'spinetbank' ),
		'items_list'            => __( 'Team Members list', 'spinetbank' ),
		'items_list_navigation' => __( 'Team Members list navigation', 'spinetbank' ),
		'filter_items_list'     => __( 'Filter team member list', 'spinetbank' ),
	);
	$rewrite = array(
		'slug'                  => '',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => false,
	);
	$args = array(
		'label'                 => __( 'Team', 'spinetbank' ),
		'description'           => __( 'Company team members', 'spinetbank' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'excerpt', 'thumbnail', 'author', 'revisions' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-businessman',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'post',
	);
	register_post_type( 'team', $args );

}
add_action( 'init', 'spinetbank_team', 0 );