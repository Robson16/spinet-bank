<?php

/**
 * Shortcode to display Team post-type
 */

function spinetbank_team_shortcode($atts)
{

    // Attributes
    $atts = shortcode_atts(
        array(
            'number_of_items'   => '6',
            'orderby'           => 'date',
            'order'             => 'ASC',
        ),
        $atts
    );

    // Custom query
    $team_items = new WP_Query(array(
        'post_type'         => 'team',
        'lang'              => substr(get_language_attributes(), 6, 2),
        'post_status'       => 'publish',
        'orderby'           => $atts['orderby'],
        'order'             => $atts['order'],
        'posts_per_page'    => $atts['number_of_items'],
    ));

    // Creating the markup
    $team_html = "<div class='team-grid'>";

    while ($team_items->have_posts()) {
        $team_items->the_post();        

        $team_html .= "<article id='post-" .  get_the_ID() . "' class='team-member'>";
        $team_html .= "<figure class='photo'>";
        $team_html .= get_the_post_thumbnail( $team_items->the_ID, 'thumbnails', array( 'alt' => get_the_title() ) );
        $team_html .= "</figure>";
        $team_html .= "<div class='infos'>";
        $team_html .= "<h3 class='title'>" . get_the_title() . "</h3>";
        $team_html .= "<h4 class='role'>" . esc_html( rwmb_meta( 'role' ) ) . "</h4>";
        $team_html .= "<hr>";
        $team_html .= "<p class='excerpt'>" . get_the_excerpt() . "</p>";

        if ( rwmb_meta( 'linkedin' ) ) {
            $team_html .= "<a class='linkedin' href='" . esc_url( rwmb_meta( 'linkedin' ) ) . "' target='_blank'>";
            $team_html .= "<span class='dashicons dashicons-linkedin'></span>";
            $team_html .= "</a>";
        }

        $team_html .= "</div>";
        $team_html .= "</article>";
    }

    $team_html .= "</div>";

    // Reset the query postdata
    wp_reset_postdata();

    return $team_html;
}
add_shortcode('team', 'spinetbank_team_shortcode');
