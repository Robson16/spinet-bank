<?php

/**
 * Shortcode to display Companies post-type
 */

function spinetbank_companies_shortcode($atts)
{

    // Attributes
    $atts = shortcode_atts(
        array(
            'number_of_items'   => '3',
            'orderby'           => 'date',
            'order'             => 'ASC',
        ),
        $atts
    );

    // Custom query
    $companies_items = new WP_Query(array(
        'post_type'         => 'companies',
        'lang'              => substr(get_language_attributes(), 6, 2),
        'post_status'       => 'publish',
        'orderby'           => $atts['orderby'],
        'order'             => $atts['order'],
        'posts_per_page'    => $atts['number_of_items'],
    ));

    // Creating the markup
    $companies_html = "<div class='companies-grid'>";

    while ($companies_items->have_posts()) {
        $companies_items->the_post();

        $logos = rwmb_meta('logo', array('limit' => 1, 'size' => 'companies-thumb'));
        $logo = reset($logos);

        $logos_alt = rwmb_meta('logo_alt', array('limit' => 1, 'size' => 'companies-thumb'));
        $logo_alt = reset($logos_alt);

        $companies_html .= "<a class='company-link' href='" . esc_url(get_permalink(get_the_ID())) . "'>";
        $companies_html .= "<article id='post-" .  get_the_ID() . "' class='company'>";
        $companies_html .= "<figure class='logo'>";

        if ($logo) {
            $companies_html .= "<img src='" . $logo['url'] . "' width='" . $logo['width'] . "' height='" . $logo['height'] . "' alt='" . $logo_alt['alt'] . "'>";
        } else {
            $companies_html .= "<img src='https://via.placeholder.com/220x90?text=Logo' width='220' height='90' alt='placeholder'>";
        }

        $companies_html .= "</figure>";
        $companies_html .= "<figure class='logo-alt'>";

        if ($logo_alt) {
            $companies_html .= "<img src='" . $logo_alt['url'] . "' width='" . $logo_alt['width'] . "' height='" . $logo_alt['height'] . "' alt='" . $logo_alt['alt'] . "'>";
        }else{
            $companies_html .= "<img src='https://via.placeholder.com/220x90?text=Logo+Alt' width='220' height='90' alt='placeholder'>";
        }

        $companies_html .= "</figure>";
        $companies_html .= "<div class='infos'>";
        $companies_html .= "<p class='excerpt'>" . get_the_excerpt() . "</p>";
        $companies_html .= "<span class='read-more'>" . __('Read more', 'spinetbank') . "</span>";
        $companies_html .= "</div>";
        $companies_html .= "</article>";
        $companies_html .= "</a>";
    }

    $companies_html .= "</div>";

    // Reset the query postdata
    wp_reset_postdata();

    return $companies_html;
}
add_shortcode('companies', 'spinetbank_companies_shortcode');
