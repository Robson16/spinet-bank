<?php

/**
 * Custom meta-boxes for Team post-type
 */

add_filter( 'rwmb_meta_boxes', 'spinetbank_team_meta_boxes' );

function spinetbank_team_meta_boxes( $meta_boxes ) {
    $prefix = '';

    $meta_boxes[] = [
        'title'    => esc_html__( 'Additional Information', 'spinetbank' ),
        'id'       => 'additional-info',
        'post_types' => ['team'],
        'context'  => 'after_title',
        'autosave' => true,
        'fields'   => [
            [
                'type'        => 'text',
                'name'        => esc_html__( 'Role', 'spinetbank' ),
                'id'          => $prefix . 'role',
                'desc'        => esc_html__( 'Role of this team member.', 'spinetbank' ),
                'placeholder' => esc_html__( 'CEO, Director, Manager...', 'spinetbank' ),
            ],
            [
                'type'        => 'url',
                'name'        => esc_html__( 'Linkedin', 'spinetbank' ),
                'id'          => $prefix . 'linkedin',
                'desc'        => esc_html__( 'LinkedIn profile URL', 'spinetbank' ),
                'placeholder' => esc_html__( 'https://www.linkedin.com/in/john-doe/', 'spinetbank' ),
            ],
        ],
    ];

    return $meta_boxes;
}