<?php 

/**
 * Custom meta-boxes for Companies post-type
 */

function spinetbank_companies_meta_boxes( $meta_boxes ) {
    $prefix = '';

    $meta_boxes[] = [
        'title'   => esc_html__( 'Logos', 'spinetbank' ),
        'id'      => 'logo',
        'post_types' => ['companies'],
        'context' => 'side',
        'fields'  => [
            [
                'type'             => 'image_advanced',
                'name'             => esc_html__( 'Logo', 'spinetbank' ),
                'id'               => $prefix . 'logo',
                'desc'             => esc_html__( 'Logo in all white version', 'spinetbank' ),
                'max_file_uploads' => 1,
            ],
            [
                'type'             => 'image_advanced',
                'name'             => esc_html__( 'Logo Alt', 'spinetbank' ),
                'id'               => $prefix . 'logo_alt',
                'desc'             => esc_html__( 'Alternative logo for mouse over effect, usually all in black', 'spinetbank' ),
                'max_file_uploads' => 1,
            ],
        ],
    ]; 

    return $meta_boxes;
}

add_filter( 'rwmb_meta_boxes', 'spinetbank_companies_meta_boxes' );