<?php

/**
 * 
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 */

// Enqueue styles and scripts for the theme
function spinetbank_scripts()
{
  // CSS
  wp_enqueue_style('dashicons');
  wp_enqueue_style('bootstrap', '//cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css', null, '4.5.0', 'all');
  wp_enqueue_style('spinetbank-webfonts', get_template_directory_uri() . '/assets/css/shared/webfonts.css', array(), wp_get_theme()->get('Version'));
  wp_enqueue_style('spinetbank-shared-styles', get_template_directory_uri() . '/assets/css/shared/shared-styles.css', array(), wp_get_theme()->get('Version'));
  wp_enqueue_style('spinetbank-frontend-styles', get_template_directory_uri() . '/assets/css/frontend/frontend-styles.css', array(), wp_get_theme()->get('Version'));

  // Js
  wp_enqueue_script('comment-reply');
  wp_enqueue_script('popper', '//cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js', array('jquery'), '1.16.0', true);
  wp_enqueue_script('bootstrap', '//cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js', array('jquery', 'popper'), '4.5.0', true);
  wp_enqueue_script('spinetbank-frontend-script', get_template_directory_uri() . '/assets/js/frontend/frontend-bundled.js', array(), wp_get_theme()->get('Version'), true);
}
add_action('wp_enqueue_scripts', 'spinetbank_scripts');

/**
 * Gutenberg scripts
 * @see https://www.billerickson.net/block-styles-in-gutenberg/
 * 
 * The webfonts need to be enqueue on this hook for the editor to be able to use them.
 */
function spinetbank_gutenberg_scripts()
{
  wp_enqueue_style('spinetbank-webfonts', get_template_directory_uri() . '/assets/css/shared/webfonts.css', array(), wp_get_theme()->get('Version'));
}
add_action('enqueue_block_editor_assets', 'spinetbank_gutenberg_scripts');

/** 
 * Set theme defaults and register support for various WordPress features.
 */
function spinetbank_setup()
{
  // Enabling translation support
  $textdomain = 'spinetbank';
  load_theme_textdomain($textdomain, get_stylesheet_directory() . '/languages/');
  load_theme_textdomain($textdomain, get_template_directory() . '/languages/');

  // Customizable logo
  add_theme_support('custom-logo', array(
    'height'      => 130,
    'width'       => 140,
    'flex-height' => false,
    'flex-width'  => false,
    'header-text' => array('site-title', 'site-description'),
  ));

  // Menu registration
  register_nav_menus(array(
    'topbar_menu_left' => __('Topbar Left', 'spinetbank'),
    'topbar_menu_right' => __('Topbar Right', 'spinetbank'),
    'main_menu' => __('Main Menu', 'spinetbank'),
    'footer_secondary_left' => __('Footer Secondary Left', 'spinetbank'),
    'footer_secondary_right' => __('Footer Secondary Right', 'spinetbank'),
    'footer_menu' => __('Footer Menu', 'spinetbank'),
  ));

  // Load custom styles in the editor.
  add_theme_support('editor-styles');
  add_editor_style(get_stylesheet_directory_uri() . '/assets/css/shared/shared-styles.css');
  add_editor_style(get_stylesheet_directory_uri() . '/assets/css/admin/admin-styles.css');

  // Enables the editor's dark mode
  add_theme_support('dark-editor-style');

  // Let WordPress manage the document title.
  add_theme_support('title-tag');

  // Enable support for featured image on posts and pages.		 	
  add_theme_support('post-thumbnails');

  // Enable support for embedded media for full weight
  add_theme_support('responsive-embeds');

  // Enables wide and full dimensions
  add_theme_support('align-wide');

  // Standard style for each block.
  add_theme_support('wp-block-styles');

  // Creates the specific color palette
  add_theme_support('editor-color-palette', array(
    array(
      'name'  => __('White', 'spinetbank'),
      'slug'  => 'white',
      'color' => '#ffffff',
    ),
    array(
      'slug'  => 'Yellow Pantone',
      'color' => '#ffe302',
    ),
    array(
      'name'  => __('Black', 'spinetbank'),
      'slug'  => 'black',
      'color' => '#000000',
    ),
  ));

  // Custom font sizes.
  add_theme_support('editor-font-sizes', array(
    array(
      'name' => __('Small', 'spinetbank'),
      'size' => 12,
      'slug' => 'small',
    ),
    array(
      'name' => __('Normal', 'spinetbank'),
      'size' => 16,
      'slug' => 'normal',
    ),
    array(
      'name' => __('Medium', 'spinetbank'),
      'size' => 20,
      'slug' => 'medium',
    ),
    array(
      'name' => __('Big', 'spinetbank'),
      'size' => 36,
      'slug' => 'big',
    ),
    array(
      'name' => __('Huge', 'spinetbank'),
      'size' => 50,
      'slug' => 'huge',
    ),
  ));

  // Custom image sizes
  add_image_size('companies-thumb', 220, 90);

  /**
   * Custom blocks styles.
   * 
   * @see https://wpblockz.com/tutorial/register-block-styles-in-wordpress/
   * @link https://developer.wordpress.org/block-editor/reference-guides/filters/block-filters/
   */
  register_block_style('core/columns', array(
    'name' => 'mobile-columns-reverse',
    'label' => __('Mobile Columns Reverse', 'spinetbank'),
  ));
  register_block_style('core/column', array(
    'name' => 'column-border-right',
    'label' => __('Border Right', 'spinetbank'),
  ));
  register_block_style('core/column', array(
    'name' => 'column-rounded-corner-border',
    'label' => __('Rounded Corner with Border', 'spinetbank'),
  ));
  register_block_style('core/paragraph', array(
    'name' => 'paragraph-rounded-corner',
    'label' => __('Rounded Corner', 'spinetbank'),
  ));
  register_block_style('core/paragraph', array(
    'name' => 'mobile-text-align',
    'label' => __('Mobile Text Align', 'spinetbank'),
  ));
  register_block_style('core/image', array(
    'name' => 'caption-on-left',
    'label' => __('Caption on the left', 'spinetbank'),
  ));
  register_block_style('core/image', array(
    'name' => 'caption-on-right',
    'label' => __('Caption on the right', 'spinetbank'),
  ));
}
add_action('after_setup_theme', 'spinetbank_setup');

/**
 * Registration of widget areas.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function spinetbank_sidebars()
{
  // Args used in all calls register_sidebar().
  $shared_args = array(
    'before_title' => '<h4 class="widget-title">',
    'after_title' => '</h4>',
    'before_widget' => '<div class="widget %2$s"><div class="widget-content">',
    'after_widget' => '</div></div>',
  );

  // Footer #1
  register_sidebar(array_merge($shared_args, array(
    'name' => __('Footer', 'spinetbank'),
    'id' => 'spinetbank-sidebar-footer-1',
    'description' => __('The widgets in this area will be displayed in Footer.', 'spinetbank'),
  )));
}
add_action('widgets_init', 'spinetbank_sidebars');

/**
 *  WordPress Bootstrap Nav Walker
 */
require_once get_template_directory() . '/includes/classes/class-wp-bootstrap-navwalker.php';

/**
 *  TGM Plugin
 */
require_once get_template_directory() . '/includes/required-plugins.php';

/**
 * Custom Post Types
 */
require_once get_template_directory() . '/includes/post-types/companies-posttype.php';
require_once get_template_directory() . '/includes/post-types/team-posttype.php';

/**
 * Custom Meta-boxes
 */
require_once get_template_directory() . '/includes/meta-boxes/companies-metabox.php';
require_once get_template_directory() . '/includes/meta-boxes/team-metabox.php';

/**
 * Shortcodes
 */
require_once get_template_directory() . '/includes/shortcodes/companies-shortcode.php';
require_once get_template_directory() . '/includes/shortcodes/team-shortcode.php';
