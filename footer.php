<?php

/**
 * The template for displaying the footer
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */

?>

<footer class="footer">
  <div class="footer-widgets">
    <div class="container flex-column align-items-center border-top pt-5">
      <?php if (is_active_sidebar('spinetbank-sidebar-footer-1')) : ?>
        <div class="widget-column">
          <?php dynamic_sidebar('spinetbank-sidebar-footer-1'); ?>
        </div>
      <?php endif; ?>
    </div>
    <!-- /.container -->
  </div>
  <!-- /.footer-widgets -->

  <div class="footer-menus">
    <div class="footer-secondary-menus">
      <div class="container d-flex flex-column flex-lg-row justify-content-md-around">
        <?php
        wp_nav_menu(array(
          'theme_location' => 'footer_secondary_left',
          'depth' => 1,
          'container_class' => 'footer-secondary-menu-wrap',
          'menu_class' => 'footer-secondary-menu footer-secondary-menu-left',
        ));
        wp_nav_menu(array(
          'theme_location' => 'footer_secondary_right',
          'depth' => 1,
          'container_class' => 'footer-secondary-menu-wrap',
          'menu_class' => 'footer-secondary-menu footer-secondary-menu-right',
        ));
        ?>
      </div>
      <!-- /.container -->
    </div>
    <!-- /.footer-secondary-menus -->

    <div class="footer-menu">
      <div class="container">
        <?php
        wp_nav_menu(array(
          'theme_location' => 'footer_menu',
          'depth' => 1,
          'container_class' => 'footer-menu-wrap',
          'menu_class' => 'footer-menu',
        ));
        ?>
      </div>
    </div>
    <!-- /.footer-menu -->
  </div>

  <div class="footer-copyright">
    <div class="container flex-column flex-lg-row justify-content-between border-top">
      <span>&copy;&nbsp;<?php echo wp_date('Y'); ?>&nbsp;<?php echo bloginfo('title'); ?>&nbsp;&ndash;&nbsp;<?php _e('All rights reserved.', 'spinetbank') ?></span>
      <span>
        <?php _e('Developed by:', 'spinetbank') ?>
        &nbsp;
        <img src="<?php echo get_template_directory_uri() . '/assets/images/vollup-brand-white.png' ?>" alt="Vollup">
      </span>
    </div>
    <!-- /.container -->
  </div>
  <!-- /.footer-copyright -->
</footer>

<?php wp_footer(); ?>

</body>

</html>