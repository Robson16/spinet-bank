<?php

/**
 * The template for displaying all single companies posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 */
get_header();
?>

<main>
  <?php
  while (have_posts()) {
    the_post();

    get_template_part('partials/companies/companies', 'single');
  }
  ?>
</main>

<div class="container">
  <div class="row">
    <div class="col">
      <?php if (comments_open() || get_comments_number()) comments_template(); ?>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</div>
<!--/.container-->

<?php
get_footer();
