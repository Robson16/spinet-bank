<?php

/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-content">
		<?php the_content(); ?>
	</div>
	<!-- /.entry-content -->

	<div class="footer-content">
    <div class="container d-flex flex-column flex-md-row justify-content-center justify-content-md-start align-items-center">     
        <span class="d-flex align-items-center mr-md-3">
          <i class="dashicons dashicons-admin-users"></i>
          <?php the_author_posts_link(); ?>&nbsp;
        </span>

        <span class="d-flex align-items-center mr-md-3">
          <i class="dashicons dashicons-calendar-alt"></i>
          <?php echo get_the_date(); ?>&nbsp;
        </span>

        <span class="d-flex align-items-center mr-md-3">
          <i class="dashicons dashicons-category"></i>
          <?php the_category(', '); ?>&nbsp;
        </span>

        <?php if ( has_tag() ) : ?>
          <span class="d-flex align-items-center mr-md-3">
            <i class="dashicons dashicons-tag"></i>
            <?php the_tags('', ', ', ''); ?>&nbsp;
          </span>
        <? endif; ?>

        <?php if ( get_comments_number() ) : ?>
          <span class="d-flex align-items-center mr-md-3">
            <i class="dashicons dashicons-admin-comments"></i>
            <?php echo get_comments_number(); ?>
          </span>
        <? endif; ?>          
    </div>
    <!-- /.container -->
	</div>
</article><!-- #post-<?php the_ID(); ?> -->