<?php

/**
 * Template part for displaying team posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="entre-header">
        <figure>
            <?php echo get_the_post_thumbnail( $team_items->the_ID, "thumbnails", array( "alt" => get_the_title() ) ); ?>
        </figure>

        <?php the_title( '<h1>', '</h1>' ); ?>

        <h2 class="role">
            <?php echo esc_html( rwmb_meta( "role" ) ); ?>
        </h2>
    </div>
    <div class="entry-content">
        <?php the_excerpt(); ?>

        <?php if ( rwmb_meta( "linkedin" ) ) : ?>
            <a href="<?php echo esc_url( rwmb_meta( "linkedin" ) ) ?>" target="_blank">
                <span class="dashicons dashicons-linkedin"></span>
            </a>
        <?php endif; ?>
    </div>
</article>
</div>
<!-- /.entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->