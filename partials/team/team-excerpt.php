<?php

/**
 * Template part for displaying team post-type excerpt in archives and search results
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 */

?>

<article id="post-<?php echo get_the_ID(); ?>" class="team-member">
    <figure class="photo">
        <a href="<?php echo esc_url( get_permalink() ); ?>">
            <?php echo get_the_post_thumbnail( $team_items->the_ID, "thumbnails", array( "alt" => get_the_title() ) ); ?>
        </a>
    </figure>
    <div class="infos">
        <a href="<?php echo esc_url( get_permalink() ); ?>">
            <h3 class="title">
                <?php echo get_the_title(); ?>
            </h3>
        </a>
        <h4 class="role"><?php echo esc_html( rwmb_meta( "role" ) ); ?></h4>
        <hr>
        <p class="excerpt"><?php echo get_the_excerpt(); ?></p>
        <?php if ( rwmb_meta( "linkedin" ) ) : ?>
            <a class="linkedin" href="<?php esc_url( rwmb_meta( "linkedin" ) ); ?>" target="_blank">
                <span class="dashicons dashicons-linkedin"></span>
            </a>
        <?php endif; ?>
    </div>
</article>