<?php

/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 */
get_header();
?>

<main>
  <div class="container" style="padding-top: 300px;">
    <div class="row">
      <div class="col">
        <?php
        while ( have_posts() ) {
          the_post();

          get_template_part( 'partials/team/team', 'single' );
        }
        ?>
      </div>
    </div>
  </div>
</main>

<?php
get_footer();
