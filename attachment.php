<?php

/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 */
get_header();
?>

<main>
    <div class="container" style="padding-top: 225px;">
        <div class="row">
            <div class="col-12 text-center">
                <?php
                while ( have_posts() ) {
                    the_post();

                    echo wp_get_attachment_image( get_the_ID(), 'large' );
                    the_title( '<h1 class="post-title mt-5">', '</h1>' );
                }
                ?>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!--/.container-->
</main>

<?php
get_footer();
