<?php

/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 */
get_header();
?>

<main style="<?php echo 'background: url(' . get_template_directory_uri() . '/assets/images/background-tall.png) center center no-repeat;'; ?> background-size: cover;">
    <section
        class="container d-flex flex-column justify-content-center text-right"
        style="min-height: 100vh";
    >
        <div class="row">
            <div class="col-12 offset-lg-6 col-lg-6">
                <span class="font-weight-bold" style="font-size: 10rem;">404</span>
                <h1 class="page-title"><?php _e( "The page you are looking for cannot be found.", 'spinetbank' ); ?></h1>
                <a class="h5" href="<?php echo get_home_url(); ?>"><?php _e( 'Return to home page', 'spinetbank' ); ?></a>
            </div>
        </div>
    </section>    
</main>

<?php
get_footer();
