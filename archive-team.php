<?php

/**
 * Archive for Team post-type
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */
get_header();
?>

<main>
    <div class="container" style="padding-top: 225px;">
        <?php if ( is_archive() ) echo sprintf( '<h1 class="display-4">%s</h1> ', get_the_archive_title() ); ?>

        <div class='team-grid'>
            <?php
            if ( have_posts() ) {
                while ( have_posts() ) {
                    the_post();
                    get_template_part( 'partials/team/team', 'excerpt' );
                }

                the_posts_pagination();
            } else {
                get_template_part( 'partials/content/content', 'none' );
            }
            ?>
        </div>
        <!-- /.team-grid -->
    </div>
    <!--/.container-->
</main>

<?php
get_footer();
