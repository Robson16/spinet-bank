<?php

/**
 * The template for displaying pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 */

get_header();
?>

<main <?php if( !wp_is_mobile() ) echo 'class="particles-container"'; ?>>

    <?php if( !wp_is_mobile() ): ?>
        <?php for ($i = 1; $i <= 100; $i++) : ?>
            <div class="circle-container">
                <div class="circle"></div>
            </div>
        <?php endfor ?>
    <?php endif ?>

    <?php
    while ( have_posts() ) {
        the_post();
        get_template_part( 'partials/content/content', 'page' );
    }
    ?>
</main>

<?php
get_footer();
